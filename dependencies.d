usage.o: usage.c include/configuration.h include/t50_help.h

cidr.o: cidr.c include/t50_config.h include/configuration.h \
 include/t50_typedefs.h include/t50_defines.h include/t50_cidr.h \
 include/t50_errors.h

cksum.o: cksum.c include/t50_cksum.h

config.o: config.c include/configuration.h include/t50_typedefs.h \
 include/t50_defines.h include/t50_config.h include/t50_netio.h \
 include/t50_errors.h include/t50_cidr.h include/t50_help.h \
 include/t50_modules.h include/protocol/t50_ip.h \
 include/protocol/t50_egp.h include/protocol/t50_gre.h \
 include/protocol/t50_rip.h include/protocol/t50_igmp.h \
 include/protocol/t50_ospf.h include/protocol/t50_rsvp.h \
 include/protocol/t50_eigrp.h include/protocol/t50_tcp_options.h

errors.o: errors.c include/configuration.h include/t50_defines.h

egp_help.o: help/egp_help.c include/t50_modules.h include/t50_typedefs.h \
 include/t50_config.h include/configuration.h include/protocol/t50_ip.h \
 include/protocol/t50_egp.h include/protocol/t50_gre.h \
 include/protocol/t50_rip.h include/protocol/t50_igmp.h \
 include/protocol/t50_ospf.h include/protocol/t50_rsvp.h \
 include/protocol/t50_eigrp.h include/protocol/t50_tcp_options.h

eigrp_help.o: help/eigrp_help.c include/t50_modules.h \
 include/t50_typedefs.h include/t50_config.h include/configuration.h \
 include/protocol/t50_ip.h include/protocol/t50_egp.h \
 include/protocol/t50_gre.h include/protocol/t50_rip.h \
 include/protocol/t50_igmp.h include/protocol/t50_ospf.h \
 include/protocol/t50_rsvp.h include/protocol/t50_eigrp.h \
 include/protocol/t50_tcp_options.h

general_help.o: help/general_help.c include/t50_modules.h \
 include/t50_typedefs.h include/t50_config.h include/configuration.h \
 include/protocol/t50_ip.h include/protocol/t50_egp.h \
 include/protocol/t50_gre.h include/protocol/t50_rip.h \
 include/protocol/t50_igmp.h include/protocol/t50_ospf.h \
 include/protocol/t50_rsvp.h include/protocol/t50_eigrp.h \
 include/protocol/t50_tcp_options.h

gre_help.o: help/gre_help.c include/t50_modules.h include/t50_typedefs.h \
 include/t50_config.h include/configuration.h include/protocol/t50_ip.h \
 include/protocol/t50_egp.h include/protocol/t50_gre.h \
 include/protocol/t50_rip.h include/protocol/t50_igmp.h \
 include/protocol/t50_ospf.h include/protocol/t50_rsvp.h \
 include/protocol/t50_eigrp.h include/protocol/t50_tcp_options.h

icmp_help.o: help/icmp_help.c include/t50_modules.h \
 include/t50_typedefs.h include/t50_config.h include/configuration.h \
 include/protocol/t50_ip.h include/protocol/t50_egp.h \
 include/protocol/t50_gre.h include/protocol/t50_rip.h \
 include/protocol/t50_igmp.h include/protocol/t50_ospf.h \
 include/protocol/t50_rsvp.h include/protocol/t50_eigrp.h \
 include/protocol/t50_tcp_options.h

igmp_help.o: help/igmp_help.c include/t50_modules.h \
 include/t50_typedefs.h include/t50_config.h include/configuration.h \
 include/protocol/t50_ip.h include/protocol/t50_egp.h \
 include/protocol/t50_gre.h include/protocol/t50_rip.h \
 include/protocol/t50_igmp.h include/protocol/t50_ospf.h \
 include/protocol/t50_rsvp.h include/protocol/t50_eigrp.h \
 include/protocol/t50_tcp_options.h

ipsec_help.o: help/ipsec_help.c include/t50_modules.h \
 include/t50_typedefs.h include/t50_config.h include/configuration.h \
 include/protocol/t50_ip.h include/protocol/t50_egp.h \
 include/protocol/t50_gre.h include/protocol/t50_rip.h \
 include/protocol/t50_igmp.h include/protocol/t50_ospf.h \
 include/protocol/t50_rsvp.h include/protocol/t50_eigrp.h \
 include/protocol/t50_tcp_options.h

ip_help.o: help/ip_help.c include/t50_modules.h include/t50_typedefs.h \
 include/t50_config.h include/configuration.h include/protocol/t50_ip.h \
 include/protocol/t50_egp.h include/protocol/t50_gre.h \
 include/protocol/t50_rip.h include/protocol/t50_igmp.h \
 include/protocol/t50_ospf.h include/protocol/t50_rsvp.h \
 include/protocol/t50_eigrp.h include/protocol/t50_tcp_options.h

ospf_help.o: help/ospf_help.c include/t50_modules.h \
 include/t50_typedefs.h include/t50_config.h include/configuration.h \
 include/protocol/t50_ip.h include/protocol/t50_egp.h \
 include/protocol/t50_gre.h include/protocol/t50_rip.h \
 include/protocol/t50_igmp.h include/protocol/t50_ospf.h \
 include/protocol/t50_rsvp.h include/protocol/t50_eigrp.h \
 include/protocol/t50_tcp_options.h

rip_help.o: help/rip_help.c include/t50_modules.h include/t50_typedefs.h \
 include/t50_config.h include/configuration.h include/protocol/t50_ip.h \
 include/protocol/t50_egp.h include/protocol/t50_gre.h \
 include/protocol/t50_rip.h include/protocol/t50_igmp.h \
 include/protocol/t50_ospf.h include/protocol/t50_rsvp.h \
 include/protocol/t50_eigrp.h include/protocol/t50_tcp_options.h

rsvp_help.o: help/rsvp_help.c include/t50_modules.h \
 include/t50_typedefs.h include/t50_config.h include/configuration.h \
 include/protocol/t50_ip.h include/protocol/t50_egp.h \
 include/protocol/t50_gre.h include/protocol/t50_rip.h \
 include/protocol/t50_igmp.h include/protocol/t50_ospf.h \
 include/protocol/t50_rsvp.h include/protocol/t50_eigrp.h \
 include/protocol/t50_tcp_options.h

tcp_udp_dccp_help.o: help/tcp_udp_dccp_help.c include/t50_modules.h \
 include/t50_typedefs.h include/t50_config.h include/configuration.h \
 include/protocol/t50_ip.h include/protocol/t50_egp.h \
 include/protocol/t50_gre.h include/protocol/t50_rip.h \
 include/protocol/t50_igmp.h include/protocol/t50_ospf.h \
 include/protocol/t50_rsvp.h include/protocol/t50_eigrp.h \
 include/protocol/t50_tcp_options.h

main.o: main.c include/configuration.h include/t50_defines.h \
 include/t50_typedefs.h include/t50_config.h include/t50_netio.h \
 include/t50_errors.h include/t50_cidr.h include/t50_memalloc.h \
 include/t50_modules.h include/protocol/t50_ip.h \
 include/protocol/t50_egp.h include/protocol/t50_gre.h \
 include/protocol/t50_rip.h include/protocol/t50_igmp.h \
 include/protocol/t50_ospf.h include/protocol/t50_rsvp.h \
 include/protocol/t50_eigrp.h include/protocol/t50_tcp_options.h \
 include/t50_randomizer.h include/t50_shuffle.h include/t50_help.h

memalloc.o: memalloc.c include/t50_defines.h include/t50_errors.h

dccp.o: modules/dccp.c include/t50_defines.h include/t50_config.h \
 include/configuration.h include/t50_typedefs.h include/t50_cksum.h \
 include/t50_memalloc.h include/t50_modules.h include/protocol/t50_ip.h \
 include/protocol/t50_egp.h include/protocol/t50_gre.h \
 include/protocol/t50_rip.h include/protocol/t50_igmp.h \
 include/protocol/t50_ospf.h include/protocol/t50_rsvp.h \
 include/protocol/t50_eigrp.h include/protocol/t50_tcp_options.h \
 include/t50_randomizer.h

egp.o: modules/egp.c include/t50_config.h include/configuration.h \
 include/t50_typedefs.h include/t50_cksum.h include/t50_memalloc.h \
 include/t50_modules.h include/protocol/t50_ip.h \
 include/protocol/t50_egp.h include/protocol/t50_gre.h \
 include/protocol/t50_rip.h include/protocol/t50_igmp.h \
 include/protocol/t50_ospf.h include/protocol/t50_rsvp.h \
 include/protocol/t50_eigrp.h include/protocol/t50_tcp_options.h \
 include/t50_randomizer.h

eigrp.o: modules/eigrp.c include/t50_defines.h include/t50_config.h \
 include/configuration.h include/t50_typedefs.h include/t50_cksum.h \
 include/t50_memalloc.h include/t50_modules.h include/protocol/t50_ip.h \
 include/protocol/t50_egp.h include/protocol/t50_gre.h \
 include/protocol/t50_rip.h include/protocol/t50_igmp.h \
 include/protocol/t50_ospf.h include/protocol/t50_rsvp.h \
 include/protocol/t50_eigrp.h include/protocol/t50_tcp_options.h \
 include/t50_randomizer.h

gre.o: modules/gre.c include/t50_defines.h include/t50_config.h \
 include/configuration.h include/t50_typedefs.h include/t50_cksum.h \
 include/t50_modules.h include/protocol/t50_ip.h \
 include/protocol/t50_egp.h include/protocol/t50_gre.h \
 include/protocol/t50_rip.h include/protocol/t50_igmp.h \
 include/protocol/t50_ospf.h include/protocol/t50_rsvp.h \
 include/protocol/t50_eigrp.h include/protocol/t50_tcp_options.h \
 include/t50_randomizer.h

icmp.o: modules/icmp.c include/t50_config.h include/configuration.h \
 include/t50_typedefs.h include/t50_cksum.h include/t50_memalloc.h \
 include/t50_modules.h include/protocol/t50_ip.h \
 include/protocol/t50_egp.h include/protocol/t50_gre.h \
 include/protocol/t50_rip.h include/protocol/t50_igmp.h \
 include/protocol/t50_ospf.h include/protocol/t50_rsvp.h \
 include/protocol/t50_eigrp.h include/protocol/t50_tcp_options.h \
 include/t50_randomizer.h

igmpv1.o: modules/igmpv1.c include/t50_defines.h include/t50_config.h \
 include/configuration.h include/t50_typedefs.h include/t50_cksum.h \
 include/t50_memalloc.h include/t50_modules.h include/protocol/t50_ip.h \
 include/protocol/t50_egp.h include/protocol/t50_gre.h \
 include/protocol/t50_rip.h include/protocol/t50_igmp.h \
 include/protocol/t50_ospf.h include/protocol/t50_rsvp.h \
 include/protocol/t50_eigrp.h include/protocol/t50_tcp_options.h \
 include/t50_randomizer.h

igmpv3.o: modules/igmpv3.c include/t50_defines.h include/t50_config.h \
 include/configuration.h include/t50_typedefs.h include/t50_cksum.h \
 include/t50_memalloc.h include/t50_modules.h include/protocol/t50_ip.h \
 include/protocol/t50_egp.h include/protocol/t50_gre.h \
 include/protocol/t50_rip.h include/protocol/t50_igmp.h \
 include/protocol/t50_ospf.h include/protocol/t50_rsvp.h \
 include/protocol/t50_eigrp.h include/protocol/t50_tcp_options.h \
 include/t50_randomizer.h

ip.o: modules/ip.c include/t50_config.h include/configuration.h \
 include/t50_typedefs.h include/t50_cksum.h include/t50_modules.h \
 include/protocol/t50_ip.h include/protocol/t50_egp.h \
 include/protocol/t50_gre.h include/protocol/t50_rip.h \
 include/protocol/t50_igmp.h include/protocol/t50_ospf.h \
 include/protocol/t50_rsvp.h include/protocol/t50_eigrp.h \
 include/protocol/t50_tcp_options.h include/t50_randomizer.h

ipsec.o: modules/ipsec.c include/t50_defines.h include/t50_config.h \
 include/configuration.h include/t50_typedefs.h include/t50_cksum.h \
 include/t50_memalloc.h include/t50_modules.h include/protocol/t50_ip.h \
 include/protocol/t50_egp.h include/protocol/t50_gre.h \
 include/protocol/t50_rip.h include/protocol/t50_igmp.h \
 include/protocol/t50_ospf.h include/protocol/t50_rsvp.h \
 include/protocol/t50_eigrp.h include/protocol/t50_tcp_options.h \
 include/t50_randomizer.h

ospf.o: modules/ospf.c include/t50_defines.h include/t50_config.h \
 include/configuration.h include/t50_typedefs.h include/t50_cksum.h \
 include/t50_memalloc.h include/t50_modules.h include/protocol/t50_ip.h \
 include/protocol/t50_egp.h include/protocol/t50_gre.h \
 include/protocol/t50_rip.h include/protocol/t50_igmp.h \
 include/protocol/t50_ospf.h include/protocol/t50_rsvp.h \
 include/protocol/t50_eigrp.h include/protocol/t50_tcp_options.h \
 include/t50_randomizer.h

ripv1.o: modules/ripv1.c include/t50_defines.h include/t50_config.h \
 include/configuration.h include/t50_typedefs.h include/t50_cksum.h \
 include/t50_memalloc.h include/t50_modules.h include/protocol/t50_ip.h \
 include/protocol/t50_egp.h include/protocol/t50_gre.h \
 include/protocol/t50_rip.h include/protocol/t50_igmp.h \
 include/protocol/t50_ospf.h include/protocol/t50_rsvp.h \
 include/protocol/t50_eigrp.h include/protocol/t50_tcp_options.h \
 include/t50_randomizer.h

ripv2.o: modules/ripv2.c include/t50_defines.h include/t50_config.h \
 include/configuration.h include/t50_typedefs.h include/t50_cksum.h \
 include/t50_memalloc.h include/t50_modules.h include/protocol/t50_ip.h \
 include/protocol/t50_egp.h include/protocol/t50_gre.h \
 include/protocol/t50_rip.h include/protocol/t50_igmp.h \
 include/protocol/t50_ospf.h include/protocol/t50_rsvp.h \
 include/protocol/t50_eigrp.h include/protocol/t50_tcp_options.h \
 include/t50_randomizer.h

rsvp.o: modules/rsvp.c include/t50_defines.h include/t50_config.h \
 include/configuration.h include/t50_typedefs.h include/t50_cksum.h \
 include/t50_memalloc.h include/t50_modules.h include/protocol/t50_ip.h \
 include/protocol/t50_egp.h include/protocol/t50_gre.h \
 include/protocol/t50_rip.h include/protocol/t50_igmp.h \
 include/protocol/t50_ospf.h include/protocol/t50_rsvp.h \
 include/protocol/t50_eigrp.h include/protocol/t50_tcp_options.h \
 include/t50_randomizer.h

tcp.o: modules/tcp.c include/t50_defines.h include/t50_config.h \
 include/configuration.h include/t50_typedefs.h include/t50_cksum.h \
 include/t50_errors.h include/t50_memalloc.h include/t50_modules.h \
 include/protocol/t50_ip.h include/protocol/t50_egp.h \
 include/protocol/t50_gre.h include/protocol/t50_rip.h \
 include/protocol/t50_igmp.h include/protocol/t50_ospf.h \
 include/protocol/t50_rsvp.h include/protocol/t50_eigrp.h \
 include/protocol/t50_tcp_options.h include/t50_randomizer.h

udp.o: modules/udp.c include/t50_config.h include/configuration.h \
 include/t50_typedefs.h include/t50_cksum.h include/t50_memalloc.h \
 include/t50_modules.h include/protocol/t50_ip.h \
 include/protocol/t50_egp.h include/protocol/t50_gre.h \
 include/protocol/t50_rip.h include/protocol/t50_igmp.h \
 include/protocol/t50_ospf.h include/protocol/t50_rsvp.h \
 include/protocol/t50_eigrp.h include/protocol/t50_tcp_options.h \
 include/t50_randomizer.h

modules.o: modules.c include/t50_defines.h include/t50_typedefs.h \
 include/t50_modules.h include/t50_config.h include/configuration.h \
 include/protocol/t50_ip.h include/protocol/t50_egp.h \
 include/protocol/t50_gre.h include/protocol/t50_rip.h \
 include/protocol/t50_igmp.h include/protocol/t50_ospf.h \
 include/protocol/t50_rsvp.h include/protocol/t50_eigrp.h \
 include/protocol/t50_tcp_options.h include/t50_errors.h \
 include/t50_shuffle.h

netio.o: netio.c include/t50_defines.h include/t50_errors.h \
 include/t50_netio.h include/t50_typedefs.h include/t50_config.h \
 include/configuration.h include/t50_randomizer.h

randomizer.o: randomizer.c include/t50_defines.h include/t50_errors.h \
 include/t50_randomizer.h include/configuration.h

shuffle.o: shuffle.c include/t50_defines.h include/t50_shuffle.h \
 include/t50_randomizer.h include/configuration.h

capabilities.o: tests/capabilities.c

cksum.o: tests/cksum.c

usage.o: usage.c include/configuration.h include/t50_help.h
