#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/syscall.h>
#include <linux/capability.h>

int main( void )
{
  struct __user_cap_header_struct chs = { .version = _LINUX_CAPABILITY_VERSION_1, .pid = getpid() };
  struct __user_cap_data_struct cds;

  // check for CAP_NET_RAW capability.
  if ( syscall( SYS_capget, &chs, &cds ) < 0 )
  {
    fputs( "ERROR using capget syscall.\n", stderr );
    return EXIT_FAILURE;
  }

  printf ( "Raw sockets %s permitted.\n", cds.permitted & CAP_NET_RAW ? "are" : "aren't" );

  return EXIT_SUCCESS;  
}
