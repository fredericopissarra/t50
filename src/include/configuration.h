/* vim: set ts=2 et sw=2 : */
/*
 *  T50 - Experimental Mixed Packet Injector
 *
 *  Copyright (C) 2010 - 2025 - T50 developers
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __CONFIGURATION_H_INCLUDED__
#define __CONFIGURATION_H_INCLUDED__

#if !defined(__GNUC__) && (__GNUC__ < 5) && (__STDC_VERSION__ < 201112)
  #error "Need GCC 5 or greater, with C11+ standard support, to compile!"
#endif

/* Name of package */
#define PACKAGE "t50"

/* Define to the version of this package. */
#define PACKAGE_VERSION "5.8.7c"

/* Define to the address where bug reports for this package should be sent. */
#define PACKAGE_BUGREPORT "https://gitlab.com/fredericopissarra/t50"

/* Define to the full name of this package. */
#define PACKAGE_NAME PACKAGE " " PACKAGE_VERSION

/* Define to the home page for this package. */
#define PACKAGE_URL "https://gitlab.com/fredericopissarra/t50.git"

/* Use fork to spawn extra process by default. Comment this for single process only. */
#define __HAVE_TURBO__

#endif
