/* vim: set ts=2 et sw=2 : */
/*
 *  T50 - Experimental Mixed Packet Injector
 *
 *  Copyright (C) 2010 - 2025 - T50 developers
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __RANDOMIZER_H__
#define __RANDOMIZER_H__

#include <stdint.h>
#include <configuration.h>

/* Randomizer macros and function */
/* NOTE: int8_t, int16_t, int32_t are synonimous of
         char, short and int. */
/* This macro will use htonX functions only if v is !0. */
/* Sometipes, v is a bitfield and NOT compatible with primitive types.
   Because of this, the default selector is necessary! */
/* RANDOM call results have not endianess! */

#define __RND(v) _Generic((v), \
                          _Bool: (!!(v) ? (v) : RANDOM()),           \
                          int8_t: (!!(v) ? (v) : RANDOM()),          \
                          int16_t: (!!(v) ? htons((v)) : RANDOM()),  \
                          int32_t: (!!(v) ? htonl((v)) : RANDOM()),  \
                          uint8_t: (!!(v) ? (v) : RANDOM()),         \
                          uint16_t: (!!(v) ? htons((v)) : RANDOM()), \
                          uint32_t: (!!(v) ? htonl((v)) : RANDOM()), \
                          default: (!!(v) ? (v) : RANDOM()))

// FIX: Random IP addresses and PORTS were reversed by __RND macro above.
#define INADDR_RND(v) ((uint32_t)(!!(v) ? (v) : RANDOM()))
#define IPPORT_RND(v) ((uint16_t)(!!(v) ? (v) : RANDOM()))

#define RANDOM random_xorshift128plus
#define SRANDOM get_random_seed

extern uint32_t random_xorshift128plus( void );
extern void get_random_seed( void );

extern uint32_t NETMASK_RND ( uint32_t );

#endif

