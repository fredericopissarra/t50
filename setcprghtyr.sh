#!/bin/bash

/usr/bin/echo -e "Change Copyright final year notice in the sources to current year.\nHit any key (Ctrl+C aborts)..."

read

YEAR="$(date +"%Y")"

for i in $(find -type f \( -name '*.c' -or -name '*.h' -or -name 'Makefile' \)); do
  if sed -i "/Copyright (C)/s/\(2010 -\) [0-9]\+/\1 ${YEAR}/" "$i"; then
    echo -e "$i \e[1;33mchanged\e[m"
  else
    echo -e "\e[1;31mERROR\e[m changing file '\e[1;33m${i}\e[m'."
  fi
done
